#!/usr/bin/env python3
import os
import argparse
import shutil


def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("filename", action='store',
                        help='.py file submit to the queue')

    parser.add_argument("-np", type=int, default=1,
                        help="Number of cores to use")

    parser.add_argument('-omp', type=int, default=1,
                        help='Number of OMP threads to use')

    return parser.parse_args()


def print_bash_script():

    sh_filename = str(args.filename.replace('.py', '.sh'))

    # Submission scripts can't start with a digit
    if sh_filename[0].isdigit():
        sh_filename = f'_{sh_filename}'

    with open(sh_filename, 'w') as bash_script:
        print('#!/bin/bash',
              '#$ -cwd',
              f'#$ -pe smp {args.np}',
              '#$ -l s_rt=360:00:00',
              f'export OMP_NUM_THREADS={args.omp}',
              'module load mpi/openmpi3-x86_64',
              f'{shutil.which("python")} {args.filename}',
              sep='\n', file=bash_script)

    return sh_filename


if __name__ == '__main__':

    args = get_args()
    if not args.filename.endswith('.py'):
        exit('Filename must end with .py')

    script_filename = print_bash_script()
    os.system(f'qsub {script_filename}')
