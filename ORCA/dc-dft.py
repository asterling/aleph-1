#!/usr/bin/env python3
"""
Run a density corrected DFT job by performing a HF single point and a DFT one SCF cycle on that density


http://tccl.yonsei.ac.kr/mediawiki/index.php/DC-DFT#How_to_Perform_HF-DFT
"""
import os
import argparse
from subprocess import Popen


def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", action='store', help='.xyz file(s) to run a HF-DFT calculation on', nargs='+')
    parser.add_argument("functional", action='store', help='DFT functional')
    parser.add_argument("charge", type=int, action='store', help="Charge")
    parser.add_argument("mult", type=int, action='store', help="Multiplicity")

    return parser.parse_args()


def xyzfile2xyz(xyz_filename):
    xyz_list = []

    with open(xyz_filename, 'r') as xyz_file:
        for line in xyz_file:
            if len(line.split()) == 4 and line.split()[-1][-1].isdigit():
                xyz_list.append(line.split())

    return xyz_list


def xys2inp(xyz_list, keywords, inp_filename, charge=0, mult=1, gbw_name=None, one_scf_iter=False):

    if gbw_name and 'MOREAD' not in keywords:
        keywords.append('MOREAD')

    if inp_filename.endswith('.inp'):
        with open(inp_filename, 'w') as inp_file:
            print('!', *keywords, file=inp_file)
            print('%maxcore 4000', file=inp_file)
            if gbw_name:
                print('%moinp "', gbw_name, '"', sep='', file=inp_file)
            if one_scf_iter:
                print('%scf MaxIter 1 end', file=inp_file)
            print('*xyz', charge, mult, file=inp_file)
            [print(*line, sep='\t', file=inp_file) for line in xyz_list]
            print('*', file=inp_file)

    return 0


def run_orca(inp_filename, out_filename):

    with open(out_filename, 'w') as orca_out:
        orca_run = Popen(['/usr/local/orca_4_1_1_linux_x86-64/orca', inp_filename], stdout=orca_out)
    orca_run.wait()

    return [line for line in open(out_filename, 'r', encoding="utf-8")]


def do_hf_calc(base_name, folder, xyz_list, arguments):

    inp_filename = base_name + '_hf.inp'
    inp_filepath = os.path.join(folder, inp_filename)

    hf_keywords = ['HF', 'RIJCOSX', 'def2-TZVP', 'def2/J', 'TIGHTSCF', 'PAL4']
    xys2inp(xyz_list, hf_keywords, inp_filepath, arguments.charge, arguments.mult)
    orca_out_lines = run_orca(inp_filepath, inp_filepath.replace('.inp', '.out'))

    nuc_energy, elec_energy = 0.0, 0.0

    for line in orca_out_lines:
        if 'Nuclear Repulsion' in line and 'ENuc' not in line:
            nuc_energy = float(line.split()[3])
            print(nuc_energy)
        if 'One Electron Energy:' in line:
            elec_energy = float(line.split()[3])
            print(elec_energy)

    return nuc_energy + elec_energy


def do_dft_calc(base_name, folder, xyz_list, arguments):

    inp_filename = base_name + '_' + arguments.functional + '.inp'
    inp_filepath = os.path.join(folder, inp_filename)

    dft_keywords = [arguments.functional, 'def2-TZVP', 'def2/J', 'TIGHTSCF', 'MOREAD', 'PAL4']
    xys2inp(xyz_list, dft_keywords, inp_filepath, arguments.charge, arguments.mult,
            gbw_name=os.path.join(folder, base_name + '_hf.gbw'),
            one_scf_iter=True)
    orca_out_lines = run_orca(inp_filepath, inp_filepath.replace('.inp', '.out'))

    two_elec_energy = 0.0

    for line in orca_out_lines:
        if 'Two Electron Energy:' in line:
            two_elec_energy = float(line.split()[3])
            print(two_elec_energy)

    return two_elec_energy


def delete_tmp(folder):
    for filename_to_rm in os.listdir(folder):
        if filename_to_rm.endswith('.tmp'):
            os.remove(os.path.join(folder, filename_to_rm))

    return 0


if __name__ == '__main__':

    args = get_args()
    dc_dft_foldername = 'dc-dft_' + args.functional
    if not os.path.exists(dc_dft_foldername):
        os.mkdir(dc_dft_foldername)

    dc_dft_energy_file = open(os.path.join(dc_dft_foldername, 'dc_dft_energies.csv'), 'a')

    for filename in args.filenames:
        if filename.endswith('.xyz'):
            xyzs = xyzfile2xyz(filename)
            basename = filename.replace('.xyz', '')
            hf_energy_part = do_hf_calc(basename, dc_dft_foldername, xyzs, args)
            dft_energy_part = do_dft_calc(basename, dc_dft_foldername, xyzs, args)
            print(basename, hf_energy_part + dft_energy_part, sep=',', file=dc_dft_energy_file)
            delete_tmp(dc_dft_foldername)

    dc_dft_energy_file.close()
