#!/usr/bin/env python3
from subprocess import Popen, PIPE


def get_gpu_util(node_name):
    """Get a dictionary of GPU ids and the corresponding utilisation"""

    proc = Popen(["ssh", str(node_name), "nvidia-smi"],
                 stdout=PIPE, stderr=PIPE)
    try:
        out, err = proc.communicate(timeout=10)
    except TimeoutError:
        proc.kill()
        return exit('Timed out')

    out_lines = [line.decode() for line in out.split(b'\n')]
    gpu_and_util = {}

    for i, line in enumerate(out_lines):

        if '|===============================+' in line:
            for j, u_line in enumerate(out_lines[i+1:]):

                # End of the GPU utilisation section
                if len(u_line.split()) == 0:
                    return gpu_and_util

                if j % 4 == 0:
                    gpu_id = str(u_line.split()[1])
                    util = float(out_lines[i+j+2].split()[-3].rstrip('%'))
                    gpu_and_util[gpu_id] = util

    return exit('Failed to find GPU utilisation')


if __name__ == '__main__':

    print(f'====================================\n'
          f'Node        GPU id   Utilisation / %\n'
          f'------------------------------------')
    for gpu_node in ('comp0800', 'comp0812'):
        for gpu_id, util in get_gpu_util(node_name=gpu_node).items():
            print(f'{gpu_node:<12s}   {gpu_id}   {util:10.1f}')
    print(f'====================================')
