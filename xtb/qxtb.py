#!/usr/bin/env python3
import os
import argparse


xtb_exe_path = '/u/fd/ball4935/opt/xtb_6.2.2/bin/xtb'
xtb_home_path = '/u/fd/ball4935/opt/xtb_6.2.2/'


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", action='store', type=str, help='.xyz file(s) sumbit to the XTB', nargs='+')
    parser.add_argument("-i", action='store', help='Optional input file to pass to XTB')

    parser.add_argument("-opt", action='store_true', default=False, help='Do an optimisation')
    parser.add_argument("-siman", action='store_true', default=False, help='Simulated annealing')
    parser.add_argument("-optfreq", action='store_true', default=False,
                        help='Perform an optimisation + frequency calculation')
    parser.add_argument("-optts", action='store_true', default=False,
                        help='Optimise to a transition state')
    parser.add_argument("-chg", action='store', default=0, type=int,
                        help='Charge of the system')
    parser.add_argument("-np", action='store', default=1, type=int,
                        help='Number of processors to use. ')
    parser.add_argument("-s", action='store', default=None, type=str,
                        help='Solvent to use')
    return parser.parse_args()


def print_bash_script(xyz_filename, args):

    qsub_filename = str(xyz_filename.replace('.xyz', '.sh'))
    out_filename = str(xyz_filename.replace('.xyz', '.out'))

    with open(qsub_filename, 'w') as bash_script:
        print('#!/bin/bash', file=bash_script)
        print('#', file=bash_script)
        print('#$ -cwd', file=bash_script)
        print('#$ -pe smp ' + str(args.np), file=bash_script)
        print('#$ -l s_rt=10:00:00', file=bash_script)
        print('ORIG=$PWD', file=bash_script)
        print('SCR=$TMPDIR', file=bash_script)
        print('cp ' + xyz_filename + ' $SCR', file=bash_script)
        if args.i is not None:
            print('cp ' + args.i + ' $SCR', file=bash_script)

        print('#', file=bash_script)
        print('cd $SCR', file=bash_script)
        print('export OMP_NUM_THREADS=' + str(args.np), file=bash_script)
        print('export XTBHOME=' + xtb_home_path, file=bash_script)

        flags = ['--chrg ' + str(args.chg)]

        if args.opt:
            flags.append('--opt')

        if args.siman:
            flags.append('--siman')

        if args.optfreq:
            flags.append('--ohess')

        if args.optts:
            flags.append('--optts')

        if args.i is not None:
            flags.append('--input ' + args.i)

        if args.s is not None:
            flags.append('--gbsa ' + str(args.s))

        print((xtb_exe_path + ' $SCR/' + xyz_filename + ' ' + ' '.join(flags) + ' > ' + out_filename), file=bash_script)
        print('cp *.out *.xyz $ORIG', file=bash_script)

    return qsub_filename


if __name__ == '__main__':

    args = get_args()
    for filename in args.filenames:
        sh_filename = print_bash_script(filename, args=args)
        os.system('qsub ' + sh_filename)
