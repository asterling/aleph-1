#!/usr/bin/python3
import sys
from subprocess import Popen, PIPE

for file in sys.argv[1:]:
    if file.split(".")[1] == "com":
        fw = open("%s_qsub.sh" % (file.split(".")[0]),"w")
        fw.write("#!/bin/csh\n\n")
        fw.write("#\n")
        fw.write("#$ -S /bin/csh\n")
        fw.write("#$ -cwd\n\n")
        fw.write("#$ -pe smp 4\n")
        fw.write("#$ -l s_rt=360:00:00\n")
        fw.write('#$ -m ea \n')
        fw.write('#$ -M alistair.sterling@chem.ox.ac.uk\n\n')
        fw.write("source /usr/local/gaussian/scripts/g09.login\n")
        fw.write("setenv ORIG $PWD\n")
        fw.write("setenv SCR $TMPDIR\n\n")
        fw.write("mkdir -p $SCR\n\n")
        fw.write("cp %s.com $SCR\n" % (file.split(".")[0]))
        fw.write("cd $SCR\n\n")
        fw.write("g09 < %s.com  >& $ORIG/%s.log\n" % (file.split(".")[0], file.split(".")[0]))
        fw.write("rm -f *.rwf\n")
        fw.write("cp * $ORIG\n")
        fw.write("cd /\n")
        fw.write("rm -Rf $SCR")
        fw.close()
        submit = Popen(["qsub", "%s_qsub.sh" % (file.split(".")[0])], stdout=PIPE)
        print(submit.stdout.readlines())
