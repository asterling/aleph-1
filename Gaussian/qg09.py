#!/usr/bin/env python3
import os
import argparse


def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", action='store', help='.inp file(s) sumbit to the queue', nargs='+')
    parser.add_argument("-ca", action='store_true', default=False,
                        help='Copy all of the files in the current dir')
    parser.add_argument("-notrashtmp", action='store_true', default=False,
                        help="Don't trash the temporary files that may be generated")
    parser.add_argument("-np", type=int, default=0, help="Override the number of cores specified in the input file")

    return parser.parse_args()


def print_bash_script(input_filename, ca, notrashtmp, np):

    if filename.endswith('.com'):
        sh_filename = str(input_filename.replace('.com', '.sh'))
        output_filename = str(input_filename.replace('.com', '.log'))
    elif filename.endswith('.gjf'):
        sh_filename = str(input_filename.replace('.gjf', '.sh'))
        output_filename = str(input_filename.replace('.gjf', '.log'))
    else:
        exit('Filename must end with .gjf or .com')

    nprocs = 1

    with open(input_filename, 'r') as inp_file:
        for line in inp_file:
            if line.startswith('%nprocs'):
                nprocs = line.split('=')[-1]

    if np != 0:
        nprocs = np

    with open(sh_filename, 'w') as bash_script:
        print('#!/bin/bash', file=bash_script)
        print('#', file=bash_script)
        print('#$ -cwd', file=bash_script)
        print('#', file=bash_script)
        print('#$ -pe smp ' + str(nprocs), file=bash_script)
        print('#$ -l s_rt=360:00:00', file=bash_script)
        print('#$ -m ea ', file=bash_script)
        print('#$ -M alistair.sterling@chem.ox.ac.uk', file=bash_script)
        print('source /usr/local/gaussian/scripts/g09.login', file=bash_script)
        print('export ORIG=$PWD', file=bash_script)
        print('export SCR=$TMPDIR', file=bash_script)
        print('mkdir -p $SCR', file=bash_script)
        if ca:
            print('cp * $SCR', file=bash_script)
        else:
            print('cp ' + input_filename + ' $SCR', file=bash_script)
        print('#', file=bash_script)
        print('cd $SCR', file=bash_script)
        print('g09 < $SCR/' + input_filename + ' >& $ORIG/' + output_filename, file=bash_script)
        if notrashtmp:
            pass
        else:
            print('rm -f *.tmp', file=bash_script)
        print('cp * $ORIG', file=bash_script)
        print('cd / ', file=bash_script)
        print('rm -Rf $SCR', file=bash_script)
        print('cd $ORIG', file=bash_script)
        print('rm *.sh.*', file=bash_script)

    return sh_filename


def run_qsub(script_filename):

    os.system('qsub ' + script_filename)

    return 0


if __name__ == '__main__':

    args = get_args()
    for filename in args.filenames:
        bash_script = print_bash_script(filename, args.ca, args.notrashtmp, args.np)
        run_qsub(bash_script)
